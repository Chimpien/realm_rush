﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour {

    Location start,end;
    [SerializeField] Color startColor, endColor, defaultColor;

    Dictionary<Vector2, Location> grid = new Dictionary<Vector2, Location>();
    Queue<Location> queue = new Queue<Location>();
    Dictionary<Location, Location> explored = new Dictionary<Location, Location>();
    List<Location> path = new List<Location>();


    Vector2Int[] directions = { Vector2Int.up,
                                Vector2Int.right,
                                Vector2Int.down,
                                Vector2Int.left};

    public List<Location> GetPath()
    {
        LoadLocations();
        BFS();
        PathFind();
        return path;
    }
    // Use this for initialization
    void Awake()
    {
        start = FindObjectOfType<WorldSettings>().GetEnemyStart();//todo make it get the start location from its spawn point (assuming I want to set spawn points for different enemies)
        end = FindObjectOfType<WorldSettings>().GetEnemyEnd();

    }

    private void PathFind()
    {
        path.Add(end);
        end.isPlacable = false;

        Location previous = end;
        while(previous != start)
        {
            path.Add(explored[previous]);
            explored[previous].isPlacable = false;
            previous = explored[previous];
        }
        
        path.Reverse();
    }

    private void BFS()
    {
        queue.Enqueue(start);
        explored.Add(start,start);

        while (queue.Count > 0)
        {
            if (queue.Contains(end))
            {
                //print("found the end");
                //print(explored.Count);
                break;
            }
            else
            {
                Location searchOrigin = queue.Dequeue();
                //print(searchOrigin.GetPosition());
                ExploreNeighbours(searchOrigin);
            }
        }
    }

    private void ExploreNeighbours(Location searchOrigin)
    {
        foreach(Vector2Int direction in directions)
        {

            Vector2Int explorationCoordinates = (searchOrigin.GetPosition() + direction);            
            try
            {                
                Location neighbour = grid[explorationCoordinates];
                if (!explored.ContainsKey(neighbour))
                {
                    Explore(neighbour, searchOrigin);
                }
            }
            catch
            {
                Debug.Log("location does not exist");
            }

        }
    }

    private void Explore(Location neighbour, Location searchOrigin)
    {
        //print("exploring " + neighbour.GetPosition());
        //neighbour.SetTopColor(Color.cyan);//color setting for debug purposes
        queue.Enqueue(neighbour);
        explored.Add(neighbour, searchOrigin);//set explored from for later path extraction        
    }            
    
    private void LoadLocations()
    {
        Location[] locations = FindObjectsOfType<Location>();
        foreach(Location location in locations)
        {
            Vector2Int position = location.GetPosition();
            if(grid.ContainsKey(position))
            {
                Debug.Log("Skipping overlapping location" + position);
                //Destroy(location.gameObject);//destroys the offending block - note the findobjects method starts its search from the bottom of the component list in the editor not the top, so the topmost duplicate(s) will be destroyed
            }
            else
            {
                grid.Add(position, location);
                if (location != start & location != end)//color the non-endpoint tops a default color                    
                    location.SetTopColor(defaultColor);
            }
            
        }
    }

  
    // Update is called once per frame
    void Update () {
		
	}
}
