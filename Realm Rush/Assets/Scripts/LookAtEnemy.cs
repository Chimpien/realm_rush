﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtEnemy : MonoBehaviour {

    Transform targetEnemy;
    [SerializeField] float range;
    ParticleSystem bullets;
    ParticleSystem.EmissionModule emissionModule;


    // Use this for initialization
    void Start ()
    {
        bullets = transform.Find("Bullets").GetComponent<ParticleSystem>();
        emissionModule = bullets.emission;
        range = range * 10 + 2;//scale range to the correct units, and add a tiny bit to get the expected behaviour
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();

        if (targetEnemy)
        {            
            transform.LookAt(targetEnemy);
            FireAtEnemy();
        }
        else
        {
            emissionModule.enabled = false;
        }
    }

    private void FindClosestEnemy()
    {
        EnemyMover[] enemies = FindObjectsOfType<EnemyMover>();
        if (enemies.Length == 0) return;
        Transform closestEnemy = enemies[0].transform;
        float shortestDistance = Vector3.Distance(transform.position, closestEnemy.position);
        
        foreach (EnemyMover enemy in enemies)
        {
            float distance = Vector3.Distance(transform.position, enemy.transform.position);
            if(distance < shortestDistance)
            {
                shortestDistance = distance;
                closestEnemy = enemy.transform;
            }
        }

        targetEnemy = closestEnemy;

    }

    private void FireAtEnemy()
    {
        //check range and decide whether to shoot        
        //print(Vector3.Distance(transform.position, enemy.position));
        if (Vector3.Distance(transform.position, targetEnemy.position) <= range)
        {
            emissionModule.enabled = true;
        }
        else
        {
            emissionModule.enabled = false;
        }
        
    }
}
