﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSettings : MonoBehaviour {

    [SerializeField] Location enemyStart;
    [SerializeField] Location enemyEnd;
    [SerializeField] public Transform towerParent;
    public Queue<Location> towerPlacements = new Queue<Location>();
    public Queue<GameObject> towers = new Queue<GameObject>();

	public Location GetEnemyStart()
    {
        return enemyStart;
    }

    public Location GetEnemyEnd()
    {
        return enemyEnd;
    }
}
