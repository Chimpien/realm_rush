﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location : MonoBehaviour {

    //simple class to contain location information for a specific cube
    [SerializeField] GameObject tower;
    [SerializeField] int towerLimit = 3;
    Transform towerParent;

    const int gridSize = 10;
    public Location exploredFrom;
    new BoxCollider collider;
    public bool isPlacable = true;
    WorldSettings worldSettings;

    // Use this for initialization
    void Start ()
    {
        collider = gameObject.GetComponentInChildren<BoxCollider>();//get the collider from the child so the size is correct, we need a collider for mouseover to work
        worldSettings = FindObjectOfType<WorldSettings>();
        towerParent = worldSettings.towerParent;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(isPlacable && !worldSettings.towerPlacements.Contains(this))
            {
                //SpawnTowerDestroyOldTower();
                SpawnOrMoveTower();
            }
            else
            {
                print("Not here!");//todo replace with playing a sound
            }
            
        }        
    }

    private void SpawnOrMoveTower()
    {
        worldSettings.towerPlacements.Enqueue(this);
        if (worldSettings.towerPlacements.Count <= towerLimit)
        {
            worldSettings.towers.Enqueue(Instantiate(tower, transform.position, Quaternion.identity, towerParent));
        }
        else
        {
            worldSettings.towerPlacements.Dequeue();
            worldSettings.towers.Peek().transform.position = transform.position;
            worldSettings.towers.Enqueue(worldSettings.towers.Dequeue());
        }
    }

    private void SpawnTowerDestroyOldTower()
    {
        print(gameObject.name);
        worldSettings.towerPlacements.Enqueue(this);
        worldSettings.towers.Enqueue(Instantiate(tower, transform.position, Quaternion.identity, towerParent));
        if (worldSettings.towerPlacements.Count > towerLimit)
        {
            worldSettings.towerPlacements.Dequeue();
            Destroy(worldSettings.towers.Dequeue());
        }
    }

    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetPosition()
    {
        return new Vector2Int(  Mathf.RoundToInt(transform.position.x / gridSize), 
                                Mathf.RoundToInt(transform.position.z / gridSize)
                             );
    }

    public void SetTopColor(Color color)
    {
        Transform top = transform.Find("Top");
        if (top != null)
        {
            MeshRenderer meshRenderer = top.GetComponent<MeshRenderer>();
            meshRenderer.material.color = color;
        }
    }
}
