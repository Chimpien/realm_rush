﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Unity.Collections;
using System.Net.Sockets;

public class EnemyMover : MonoBehaviour {

    //[SerializeField] List<Location> path;
    //SocketServer socketServerAsync;

    // Use this for initialization
    void Start ()
    {
        //socketServerAsync = GetComponent<SocketServer>();
        PathFinder pathFinder = gameObject.GetComponent<PathFinder>();
        List<Location> path = pathFinder.GetPath();//todo this should happen in update, but only if the player has made any changes (avoid unneccesary replanning)
        StartCoroutine(FollowPath(path));//start a co-routine running
    }

    IEnumerator FollowPath(List<Location> path)
    {
        print("starting patrol");
        foreach (Location location in path)
        {
            //print(location.name);
            //SendMsgOnSocket(location);
            transform.position = location.transform.position;            
            yield return new WaitForSeconds(2f);//add a request to come back to here to Unity execution queue at current time + specified value in seconds, and return to where coroutine was started
        }
        print("ending patrol");
    }

    /*private void SendMsgOnSocket(Location location)
    {
        Socket handler = socketServerAsync.getSocket();
        Debug.Log(handler);
        
        if (handler != null)
        {
            byte[] msg = Encoding.ASCII.GetBytes(location.name);

            handler.Send(msg);
        }

    }*/

    // Update is called once per frame
    void Update ()
    {
		
	}
}
