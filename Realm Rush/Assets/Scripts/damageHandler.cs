﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageHandler : MonoBehaviour {

    [SerializeField] int hitPoints = 10;
    new Collider collider;

	// Use this for initialization
	void Start () {

        collider = transform.Find("Body").GetComponent<Collider>();//collider is on the body so that it gets its size info from the body size, need to find it so that the collider script works from the enemy container
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();
        if(hitPoints < 1)
        {
            DestroyEnemy();
        }
    }

    private void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    private void ProcessHit()
    {        
        hitPoints -= 1;
        print("HP = " + hitPoints);
    }
}
