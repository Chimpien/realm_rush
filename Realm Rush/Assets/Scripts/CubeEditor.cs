﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Location))]//enforce dependency on named script, so one gets auto added if it is missing
[ExecuteInEditMode]//script does stuff in the scene editor rather than in game
[SelectionBase]//initial click within the prefab selects the whole object not individual components
public class CubeEditor : MonoBehaviour {

    //class to cause the cubes to snap to a grid and be labelled with their position in the grid
    Location location;

    private void Awake()
    {
        //get a handle to the location script
        location = GetComponent<Location>();//when first added caused a bug with awake not being called. Doesn't seem to happen with new instances though
    }
    
	// Update is called once per frame
	void Update ()
    {
        SnapToPosition();
        SetText();
    }

    private void SnapToPosition()
    {
        float snapX, snapZ;
        //get the location in grid coords and scale to editor units
        snapX = location.GetPosition().x * location.GetGridSize();
        snapZ = location.GetPosition().y * location.GetGridSize();
        transform.position = new Vector3(snapX, 0, snapZ);//set the position
    }

    private void SetText()
    {
        TextMesh textMesh = GetComponentInChildren<TextMesh>();
        textMesh.text = location.GetPosition().x + "," + location.GetPosition().y;//put the co-ords in the text on the cube top
        gameObject.name = "Cube " + textMesh.text;//use the co-ords to set the object name
    }
}
