﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    [SerializeField] EnemyMover enemy;
    [SerializeField] float secondsBetweenSpawns = 2f;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(SpawnEnemy());
	}

    private IEnumerator SpawnEnemy()
    {
        for (int i = 0; i < 4; i++)
        {
            print("Spawn " + i);
            EnemyMover newEnemy = Instantiate(enemy, transform.position, Quaternion.identity, transform);
            //newEnemy.transform.parent = transform;
            yield return new WaitForSeconds(secondsBetweenSpawns);
        }
    }
    
}
